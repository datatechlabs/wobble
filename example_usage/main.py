#!/usr/bin/env python
"""
Main example script, search for ``MODAPP'' to find the code that would normally
have to be modified for a specific application.
"""

import sys, getopt, signal
sys.path.append("..")	# MODAPP, only needed if wobble is not installed
from wobble import slog, web, templates, modules, conf, tools

# MODAPP, path to configuration file
config_file = "./wobble.conf"

# MODAPP, map url regexps to module.function
urlmap = [
	("/overview",  "various.overview"),
	("/login", "various.login"),
	("/logout", "various.logout"),
]

parsed_urlmap = []

def main():
	global parsed_urlmap, config_file, module_dir
	
	# parse command-line options
	try:
		(opts, args) = getopt.getopt(sys.argv[1:], "hdDc:i:p:t:")
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	ip_address = None
	port = None
	server_type = None
	daemon = None
	debug_mode = None
	for (opt, val) in opts:
		if opt == "-h":
			usage()
			sys.exit()
		if opt == "-c":
			config_file = val
		if opt == "-i":
			ip_address = val
		if opt == "-p":
			port = val
		if opt == "-t":
			server_type = val
		if opt == "-d":
			daemon = True
		if opt == "-D":
			debug_mode = True

	# read configuration file
	conf.read(config_file)
	conf.set_value("PATHS", "config_file", config_file)
	if ip_address != None:
		conf.set_value("SERVER", "ip_address", ip_address)
	if port != None:
		conf.set_value("SERVER", "port", port)
	if server_type != None:
		conf.set_value("SERVER", "type", server_type)
	if daemon != None:
		conf.set_value("SERVER", "daemon", daemon)

	# alter state if debugging mode is on
	if debug_mode:
		conf.set_value("SERVER", "daemon", False)
		conf.set_value("SERVER", "log_to_file", False)

	# set logging stream
	log_to_file = conf.get_bool("SERVER", "log_to_file")
	if log_to_file:
		log_file = conf.get_str("PATHS", "log_file")
		set_stream(log_file)

	# set logging levels
	slog.output_tags(conf.get_list("SERVER", "log_tags"))

	# daemonize process
	daemon = conf.get_bool("SERVER", "daemon")
	if daemon:
		tools.daemonize()

	# dump configuration
	slog.info("configuration", conf.get_all())

	register_signal_handlers()

	# import web module
	server_type = conf.get_str("SERVER", "type")
	web.set_interface(server_type)

	# parse urlmap
	module_dir = conf.get_str("PATHS", "module_dir")
	parsed_urlmap = web.parse_urlmap(urlmap, module_dir)

	# compile templates
	template_dir = conf.get_str("PATHS", "template_dir")
	templates.init(template_dir)

	# switch user/group
	user = conf.get_str("SERVER", "user")
	group = conf.get_str("SERVER", "group")
	tools.switch_user(user, group)

	# MODAPP, connect to database
	#sql_dir = conf.get_str("PATHS", "sql_dir")
	#db.shared_connections = False
	#db.read_queries(sql_dir)
	#db.connect("mysql", user="wobble", database="wobble")

	# initialize and run web server
	ip_address = conf.get_str("SERVER", "ip_address")
	port = conf.get_int("SERVER", "port")
	web.init(handle_request, ip_address, port)
	web.run()

def handle_request():
	"""Function that is called upon each HTTP request."""
	# get request uri and remove query string
	req_uri = web.getvar_env("REQUEST_URI", "")
	slog.info("request uri:", req_uri)
	pos = req_uri.find('?')
	if pos != -1:
		req_uri = req_uri[:pos]

	# find the module.function that matches, if any
	match = None
	subgroups = ()
	for regexp, module_name, function in parsed_urlmap:
		match = regexp.search(req_uri)
		if match == None:
			continue
		subgroups = match.groups()
		break

	# check if we found a matching function
	if match == None:
		web.error_not_found()
		return

	# execute module function (user callback)
	slog.info("executing function", function.__name__, "from module", module_name)
	function(*subgroups)

	# MODAPP, for applications with database support:
	# try:
	#	function(*subgroups)
	# except:
	#	slog.err("user module produced an exception")
	#	slog.err(traceback.format_exc())
	#
	# db.free_connection()
	
	web.set_default_headers()

def usage():
	"""Print usage info to stdout."""
	print "wobble application"
	print
	print "\t-h\t\t\tthis help message"
	print "\t-c config_file\t\tconfiguration file path"
	print "\t-i ip_address\t\tIP address to listen on"
	print "\t-p port\t\t\tport number"
	print "\t-t server_type\t\tserver type (standalone, fastcgi)"
	print "\t-d\t\t\tdaemonize the process"
	print "\t-D\t\t\tdebug mode (run in foreground, log to screen, be verbose)"
	print

def handle_kill(signum, frame):
	slog.info("shutting down..")
	sys.exit()

def handle_hup(signum, frame):
	"""Reload modules and templates that have changed on disk."""
	global parsed_urlmap
	
	templates.reload()
	modules.reload()
	parsed_urlmap = web.parse_urlmap(urlmap, module_dir)

def register_signal_handlers():
	"""Register signal handler functions."""
	signal.signal(signal.SIGINT, handle_kill)
	signal.signal(signal.SIGTERM, handle_kill)
	signal.signal(signal.SIGHUP, handle_hup)

# run the script
main()

