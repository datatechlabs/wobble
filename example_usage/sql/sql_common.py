
fetch_user = """
SELECT * FROM users WHERE screen_name = %(screen_name)s
"""

select_messages = """
SELECT * FROM my_messages
"""

#
# sessions
#

create_session = """
INSERT INTO websessions (
	session_id,
	user_id,
	ip_address,
	exp_timestamp,
	timeout,
	additional_data
)
VALUES (
	%(session_id)s,
	%(user_id)s,
	%(ip_address)s,
	%(exp_timestamp)i,
	%(timeout)i,
	%(data)s
)
"""

find_session = """
SELECT * FROM websessions WHERE session_id=%(session_id)s
"""

update_session = """
UPDATE websessions
SET
	exp_timestamp = %(exp_timestamp)i,
	additional_data = %(data)s
WHERE
	session_id = %(session_id)s
"""

delete_session = """
DELETE FROM websessions WHERE session_id=%(session_id)s
"""

delete_expired_sessions = """
DELETE FROM websessions WHERE strftime('%s', CURRENT_TIMESTAMP) >= exp_timestamp
"""
