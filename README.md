Wobble
==============

A collection of python scripts to simplify work with databases, logging, 
configuration file parsing. It even features simple webserver.

Its primary use is in [Datatechlabs][datatechlabs] products, but it can be used in any python
project.

Installation
------------

For production, please only use most recent **tag**

    tar -xvzf wobble.X.Y.Z.tgz
    cd wobble_X_Y_Z
    python setup.py install

    
Example usage
-------------

Here is small example which demonstrates usage of wobble's slog, conf and db 
modules. 
Create 2 files: 

`test.py` - the test script

	#!/usr/bin/env python
	
	from wobble import slog, conf, db
	
	# read config file
	
	config_file = "test.conf"
	conf.read(config_file)
	
	# print all configuration on screen
	slog.info("dumping configuration:", conf.get_all())
	
	# connect to database (this will fail if you haven't set the database yet
	# but shows the concept of getting config attributes
	
	db_enable = conf.get_bool('DATABASE', 'enable')
	db_name = conf.get_str('DATABASE', 'name')
	db_host = conf.get_str('DATABASE', 'host')
	db_user = conf.get_str('DATABASE', 'user')
	db_pass = conf.get_str('DATABASE', 'pass')
	
	# connect to database. You can have as many handles as you like
	db_handle = "connection1"
	
	try:
		db.connect("mysql", db_handle, db_host, db_user, db_name, db_pass)
	except:
		# print an error using slog module
		slog.err("Database connection failed")
	
	# you can change the configuration on the fly:
	slog.info("Database enable is ", conf.get_bool('DATABASE', 'enable'))
	
	conf.set_value('DATABASE', 'enable', 'false')
	
	# print again
	slog.info("Database enable now is ", conf.get_bool('DATABASE', 'enable'))    

`test.conf` - the test config file

	[DATABASE]

	enable = True
	name = "test_db"
	host = "localhost"
	user = "someuser"
	pass = "somepass"
	
	[DEBUG]
	
	log_levels = "error,warning,info,verbose,db"
	log_file = "/path/to/log/file.log"
   
	
And now run the script: `python test.py`

To see more, please consult `example_usage` directory for web server example with
template parser.
	
    
[datatechlabs]: http://datatechlabs.com
