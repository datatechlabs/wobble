"""
Various utility functions.
"""

from decimal import Decimal
import os, errno, re, time, datetime, random, pwd, grp, threading, traceback
import slog

#
# process/file management
#

def switch_user(user="", group=""):
	"""Change process user/group IDs."""
	if group:
		gid = grp.getgrnam(group)[2]
		os.setgid(gid)
	if user:
		uid = pwd.getpwnam(user)[2]
		os.setuid(uid)

def daemonize():
	pid = os.fork()
	if pid != 0:
		# kill first parent
		os._exit(0)
		
	os.setsid()
	pid = os.fork()
		
	if pid != 0:
		# kill second parent
		os._exit(0)
		
	#os.chdir("/")
	#os.umask(0)

def change_owner(path, user=None, group=None):
	"""Change the owner of a file or directory."""
	
	if user != None:
		uid = pwd.getpwnam(user)[2]
		os.chown(path, uid, -1)
	if group != None:
		gid = grp.getgrnam(group)[2]
		os.chown(path, -1, gid)

def make_dir(path):
	"""Create a directory. Returns False if the directory already exists,
	True otherwise. Raises an exception if something went wrong.
	"""
	try:
		os.makedirs(path)
	except OSError, err:
		if err.errno == errno.EEXIST:
			return False

		raise

	return True

def makefile(path):
	"""Create an empty file if it does not already exist."""
	
	(head, tail) = os.path.split(path)
	if head:
		make_dir(head)
	if not os.access(path, os.F_OK):
		open(path, "w").close()

def safetouch(path):
	"""Update file's last modification time. If the file is not there, do
	nothing.
	"""
	try:
		os.utime(path, None)
	except OSError, err:
		if err.errno == errno.ENOENT:
			return
		raise

def make_pid_file(path, user, group):
	"""Create process ID file."""
	
	try:
		pid = os.getpid()
		pidfile = open(path, "w")
		pidfile.write(str(pid))
		pidfile.close()
		
		# change owner
		change_owner(path, user, group)
	except:
		slog.err("Cannot create PID file")
		slog.err(traceback.format_exc())

#
# strings
#

# some characters for generating random strings
allchars = "abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ"

def randstr(length=50):
	"""Produce a string of random characters."""	
	ret = ""
	for i in xrange(length):
		ret += random.choice(allchars)
	return ret

def cut_prefix(prefix, source):
	"""If source starts with prefix, cut it."""
	if source.startswith(prefix):
		return source[len(prefix):]
	return source

def splice(base_str, mixin, interval):
	""" '12345' spliced with '-' using interval=2
	produces '12-34-5'
	"""
	spliced = ""
	for i in xrange(0, len(base_str)-interval, interval):
		spliced += base_str[i:i+interval] + mixin

	i += interval
	spliced += base_str[i:i+interval]
	return spliced

def nl2br(text):
	return text.replace("\n", "<br />")

#
# webform
#
			
def option_list_db(result_set, key_attr, value_attr):
	"""Prepare a list for use in a web form <select> field from a database
	cursor.
	"""
	select_list = []
	while True:
		row = result_set.fetchone()
		if row == None:
			break
		pair = (getattr(row, key_attr), getattr(row, value_attr))
		select_list.append(pair)
		
	return select_list

def option_list_mirror(option_list):
	"""Prepare a list for use in a web form <select> field by mirroring
	values as keys.
	"""
	select_list = []
	for option in option_list:
		select_list.append((option, option))

	return select_list
		
def first_day():
	"""Return ISO timestamp representation of the first day of the
	current month.
	"""
	return time.strftime("%Y-%m-01")

def last_day():
	"""Return ISO timestamp representation of the last day of the
	current month.
	"""
	now = time.localtime()

	year = now[0]
	nextmonth = now[1] + 1
	
	if nextmonth > 12:
		nextmonth = 1
		year += 1
		
	ed = datetime.date(year, nextmonth, 1) - datetime.timedelta(1)
	return ed.isoformat()

def iso_ts_fmt(iso_ts, fmt="%d/%m/%y %H:%M"):
	"""Format ISO timestamp."""
	iso_ts = str(iso_ts)
	iso_ts = iso_ts[:19] # cut off milli/micro seconds

	parsed = time.strptime(iso_ts, "%Y-%m-%d %H:%M:%S")
	return time.strftime(parsed, fmt)

def sec_fmt(seconds, fmt="%H:%M:%S"):
	"""Split seconds into hours minutes and such."""
	hours = seconds/60/60
	minutes = (seconds - (hours*60*60))/60
	seconds = seconds - (hours*60*60) - (minutes*60)
	
	fmt = fmt.replace("%H", str(hours).rjust(2, '0'))
	fmt = fmt.replace("%M", str(minutes).rjust(2, '0'))
	fmt = fmt.replace("%S", str(seconds).rjust(2, '0'))

	return fmt
	

#
# RADIUS
#

def radparse(_rad_params):
	# convert keys to lowercase, strip junk from values
	rad_params = {}
	
	# lower priority attributes which should not overwrite others
	lowpri_dict = {}

	# convert attribute names to lowercase
	for key, value in _rad_params.iteritems():
		if key.startswith('Quintum-'):
			# strip 'Quintum-' and store as lower priority
			lowpri_dict[key[8:].lower()] = value
			continue
				
		key = key.lower()
		rad_params[key] = value

	# overwrite lower priority attributes
	lowpri_dict.update(rad_params)
	rad_params = lowpri_dict

	# format h323 conference id
	if "h323-conf-id" in rad_params:
		rad_params["h323-conf-id"][0] = format_h323_conf_id(rad_params["h323-conf-id"][0])

	# convert time from CISCO format to internal datetime objects
	if "h323-setup-time" in rad_params:
		rad_params["h323-setup-time"][0] = parse_datetime(rad_params["h323-setup-time"][0])
	if "h323-connect-time" in rad_params:
		rad_params["h323-connect-time"][0] = parse_datetime(rad_params["h323-connect-time"][0])
	if "h323-disconnect-time" in rad_params:
		rad_params["h323-disconnect-time"][0] = parse_datetime(rad_params["h323-disconnect-time"][0])
		
	# remove leading "sip:" from phone numbers
	if "calling-station-id" in rad_params:
		rad_params["calling-station-id"][0] = cut_prefix("sip:", rad_params["calling-station-id"][0])
	if "called-station-id" in rad_params:
		rad_params["called-station-id"][0] = cut_prefix("sip:", rad_params["called-station-id"][0])

	return rad_params

def format_h323_conf_id(conf_id):
	parts = conf_id.split(' ')
	if len(parts) != 4:
		raise Exception, "H323 conference ID format incorrect: " + conf_id

	ret = ""
	for p in parts:
		p = "00000000" + p
		ret += p[-8:]

	return ret

def getfirst(rad_dict, key, default=None):
	try:
		return rad_dict[key][0]
	except:
		return default

# ISO example: "2006-10-17 18:27:33.232375"
# Cisco example: "17:57:09.155 UTC Thu May 18 2006"
# Cisco variant with TZ (Sansay switch) example: "09:11:23.570 UTC-8 Wed Apr  8 2009"
# Quintum example: "Thu, 08 Nov 2007 07:12:34"
iso_format = re.compile("^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})\.?(\d+)?$")
cisco_format = re.compile("^\W?(\d{1,2}):(\d{1,2}):(\d{1,2})\.?(\d+)?\s+([a-zA-Z0-9-]+)\s+(\w{3})\s+(\w{3})\s+(\d+)\s+(\d{1,4})$")
quintum_format = re.compile("^(\w+),?\s+(\d+)\s+(\w{3})\s+(\d{1,4})\s+(\d{1,2}):(\d{1,2}):(\d{1,2})\.?(\d+)?$")

def parse_datetime(datestr):
	"""Parse datetime from Cisco or ISO format to python's internal
		datetime class.
		Input: (string) datetime string
		Output: (datetime.datetime) parsed date and time
	"""
	mo = cisco_format.match(datestr)
	if mo:
		groups = mo.groups()
		mstr = "JanFebMarAprMayJunJulAugSepOctNovDec"

		year = int(groups[8])
		month = mstr.find(groups[6]) / 3 + 1
		day = int(groups[7])
		hour = int(groups[0])
		min = int(groups[1])
		sec = int(groups[2])
		fraction = groups[3]

		# convert time fractions to microseconds
		# (they could be both milli and micro seconds)
		if not fraction:
			microsec = 0
		elif len(fraction) <= 3:
			microsec = int(fraction) * 1000
		else:
			microsec = int(fraction)

		dt = datetime.datetime(year, month, day, hour, min, sec, microsec)
		return dt

	mo = iso_format.match(datestr)
	if mo:
		groups = mo.groups()
		year = int(groups[0])
		month = int(groups[1]) 
		day = int(groups[2])
		hour = int(groups[3])
		min = int(groups[4])
		sec = int(groups[5])
		fraction = groups[6]

		# convert time fractions to microseconds
		# (they could be both milli and micro seconds)
		if not fraction:
			microsec = 0
		elif len(fraction) <= 3:
			microsec = int(fraction) * 1000
		else:
			microsec = int(fraction)

		dt = datetime.datetime(year, month, day, hour, min, sec, microsec)
		return dt

	mo = quintum_format.match(datestr)
	if mo:
		groups = mo.groups()
		mstr = "JanFebMarAprMayJunJulAugSepOctNovDec"

		year = int(groups[3])
		month = mstr.find(groups[2]) / 3 + 1
		day = int(groups[1])
		hour = int(groups[4])
		min = int(groups[5])
		sec = int(groups[6])
		fraction = groups[7]

		# convert time fractions to microseconds
		# (they could be both milli and micro seconds)
		if not fraction:
			microsec = 0
		elif len(fraction) <= 3:
			microsec = int(fraction) * 1000
		else:
			microsec = int(fraction)

		dt = datetime.datetime(year, month, day, hour, min, sec, microsec)
		return dt

	raise Exception, "invalid date format"

def moneyfmt(value, places=3, curr='', sep='', dp='.',
             pos='', neg='-', trailneg=''):
    """Convert Decimal to a money formatted string.

    places:  required number of places after the decimal point
    curr:    optional currency symbol before the sign (may be blank)
    sep:     optional grouping separator (comma, period, space, or blank)
    dp:      decimal point indicator (comma or period)
             only specify as blank when places is zero
    pos:     optional sign for positive numbers: '+', space or blank
    neg:     optional sign for negative numbers: '-', '(', space or blank
    trailneg:optional trailing minus indicator:  '-', ')', space or blank

    Copied from here: http://docs.python.org/lib/decimal-recipes.html
    """
    # my modifications to allow other types
    if not isinstance(value, Decimal):
	    value = Decimal(str(value))
    
    q = Decimal((0, (1,), -places))    # 2 places --> '0.01'
    sign, digits, exp = value.quantize(q).as_tuple()
    assert exp == -places    
    result = []
    digits = map(str, digits)
    build, next = result.append, digits.pop
    if sign:
        build(trailneg)
    for i in range(places):
        if digits:
            build(next())
        else:
            build('0')
    build(dp)
    i = 0
    while digits:
        build(next())
        i += 1
        if i == 3 and digits:
            i = 0
            build(sep)
    build(curr)
    if sign:
        build(neg)
    else:
        build(pos)
    result.reverse()

    # My modifications to avoid dot (.) in front of strings. It seems to be
    # easier to read when there's a zero there.
    result = ''.join(result)
    if result[0] == ".":
	    return "0" + result
    if result[:2] == "-.":
	    return "-0" + result[1:]

    return result

def convert_money(amount, from_rate, to_rate=Decimal(1)):
        """Convert amount from from_rate to to_rate."""
        return amount * Decimal(str(to_rate)) / Decimal(str(from_rate))

# global unique ID counter and its lock
_uid = 1
_uid_lock = threading.Lock()

def unique_id():
	"""Generate a unique ID."""
	
	global _uid, _uid_lock
	
	_uid_lock.acquire()
	
	current_id = _uid
	_uid += 1
	
	_uid_lock.release()
	
	return current_id
