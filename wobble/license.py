"""License registration/verification."""

import os, traceback, httplib, md5
import slog, conf, tools

# license registration status codes
REG_SUCCESS = 0
REG_NOLICENSE = 1
REG_BADLICENSE = 2
REG_NOHWINFO = 3
REG_SERVFAIL = 4

def write_file(filename, data_dict, overwrite=False):
	"""Write license data to the license file."""
	if overwrite:
		mode = 'w'
	else:
		mode = 'a'
	
	fp = file(filename, mode)
	fp.write("\n")
	for key, value in data_dict.iteritems():
		fp.write("%s = %s\n" % (key, value))
	fp.close()

def format_hash(hash):
	"""Make some hash value uppercase and split it into multiple parts."""
	return tools.splice(hash.upper(), '-', 6)

def parse_hash(displayed):
	"""Perform the reverse of format_hash()."""
	return displayed.replace('-', '').lower().strip()

def perm_key_cmp(key, serial, hw_finger):
	"""Create a key from `serial` number and `hw_finger` (hardware
	fingerprint) and compare it with the provided registration `key`

	key -- registration key
	serial -- software serial number
	hw_finger -- hardware fingerprint

	Return hash(serial + hw_finger) == key
	"""
	slog.info("key comparison (key, serial, hw_finger):")
	slog.info(key, serial, hw_finger)
	
	gen_key = serial + "WAWAWEEWA" + hw_finger
	gen_key = md5.new(gen_key).hexdigest()
	if key == gen_key:
		slog.info("key comparison successful")
		return True
	
	slog.info("key comparison failed")
	return False

def reg_perm(license_file, serial, hw_finger):
	"""Register a permanent license.

	license_file -- path to the license file
	serial -- software serial number
	hw_finger -- hardware fingerprint

	Returns a registration status code
	"""
	slog.info("permanent registration")
	slog.info("license file:", license_file)
	slog.info("serial:", serial)
	slog.info("hardware fingerprint:", hw_finger)
	
	# send serial number and hardware info to registration server
	reg_query = "/perm/%s/%s" % (serial, hw_finger)
	slog.info("querying regserver:", reg_query)
	try:
		reg_conn = httplib.HTTPConnection("regserver.datatechlabs.com", 8123)
		reg_conn.request("GET", reg_query)

		# read license hash from the server
		resp = reg_conn.getresponse()
		server_hash = resp.read().strip()
	except:
		slog.warn("communication with regserver failed")
		slog.warn(traceback.format_exc())
		return REG_SERVFAIL

	if perm_key_cmp(server_hash, serial, hw_finger):
		data = {
			"type": "permanent",
			"software": "megabox",
			"version": "any",
			"features": "all",
			"serial": serial,
			"key": format_hash(server_hash)
		}
		slog.info("registration successful, writing license file")
		write_file(license_file, data, True)
		return REG_SUCCESS

	slog.info("registration failed")
	return REG_BADLICENSE

def check_perm(license_file, hw_finger):
	"""Check permanent license.

	license_file -- location of the license file
	hw_finger -- hardware fingerprint

	Returns a registration status code
	"""
	# read license file
	try:
		conf.read(license_file, alias="LICENSE")
	except:
		slog.warn("license file not found")
		slog.warn(traceback.format_exc())
		return REG_NOLICENSE

	# get serial number and registration key from license
	key = conf.get_str("UNNAMED", "key", "", "LICENSE")
	serial = conf.get_str("UNNAMED", "serial", "", "LICENSE")

	key = parse_hash(key)

	if perm_key_cmp(key, serial, hw_finger):
		return REG_SUCCESS

	return REG_BADLICENSE

def check_std(license_file, hw_finger, sessid=None):
	"""Check license the standard way.

	license_file -- path to the license file
	hw_finger -- hardware fingerprint
	sessid -- some session ID (optional)

	Returns a registratrion status code
	"""
	return REG_SUCCESS

	# generate a session id if one was not provided
	if sessid == None:
		sessid = tools.randstr()

	# read license file
	try:
		conf.read(license_file)
	except:
		slog.warn("license file not found")
		return REG_NOLICENSE
	
	license_id = conf.get_str("UNNAMED", "id")
	software = conf.get_str("UNNAMED", "software")
	version = conf.get_str("UNNAMED", "version")
	features = conf.get_str("UNNAMED", "features")
	client = conf.get_str("UNNAMED", "client")
	serial = conf.get_str("UNNAMED", "serial")

	if hw_info == "":
		slog.warn("cannot extract hardware fingerprint")
		return REG_NOHWINFO

	# send license id, hardware info and session id to registration server
	reg_query = "/std/%s/%s/%s" % (license_id, hw_info, sessid)

	try:
		reg_conn = httplib.HTTPConnection("regserver.datatechlabs.com", 8123)
		reg_conn.request("GET", reg_query)

		# read license hash from the server
		resp = reg_conn.getresponse()
		server_hash = resp.read().strip()
	except:
		slog.warn("communication with regserver failed")
		return REG_SERVFAIL

	slog.info("reg query:", reg_query)
	slog.info("reg status and reply:", resp.status, " ", server_hash)

	# create the complete license string.
	# this operatation is performed both here and on the server side.
	license_string = serial
	license_string += sessid
	license_string += hw_finger
	license_string += version
	license_string += software
	license_string += features

	# hash the license string
	client_hash = md5.new(license_string).hexdigest()
	
	slog.info("license string:", license_string)
	slog.info("client hash:", client_hash)
	slog.info("server hash:", server_hash)

	# compare license strings
	if (resp.status != 200) or (client_hash != server_hash):
		slog.warn("regserver reports invalid license")
		return REG_BADLICENSE

	return REG_SUCCESS
