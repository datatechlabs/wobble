"""
ulog -- module for logging events that user should definitely be aware of.

Unlike slog, which logs its messages to a simple stream and is intended for use
by a system's administrator, ulog messages can go anywhere. Currently though,
only the database option is supported.
"""

import slog, db

_source = "UNKNOWN"

def set_source(name):
	global _source
	_source = name

def write(level, msg, data=None, connection="default"):
	global _source

	p = {
		"message": msg,
		"data": data,
		"level": level,
		"source": _source
	}
	db.execute("log_event", p, connection)

def warn(*msg):
	"""Warning message."""
	
	write(1, *msg)

def err(*msg):
	"""Error message."""
	
	write(2, *msg)

def info(*msg):
	"""Info message. Avoid this."""
	
	write(0, *msg)
