"""Gather various data from the operating system."""

import os, re, md5
import slog, conf

def network_interfaces():
	"""Gather data on all network interfaces."""
	
	# invoke ifconfig
	ifconfig = os.popen("ifconfig")

	os_type = conf.get_str("DEVICE", "os_type", "freebsd")
	# ifconfig output line matching regexps
	if os_type == "freebsd":
		if_startline_re = re.compile("^(\w+):")
		inet_line_re = re.compile("inet\s+([0-9.]+)\s+netmask\s+([\w.]+)")
		ether_line_re = re.compile("ether\s+([\w:]+)")
	elif os_type == "debian":
		if_startline_re = re.compile("^([a-zA-Z_0-9:]+)\s+.*Ethernet")
		inet_line_re = re.compile("inet\s+addr:([0-9.]+)\s+Bcast:[0-9.]+\s+Mask:([0-9.]+)")
		ether_line_re = re.compile("Ethernet\s+Hwaddr\s+([\w:]+)")
	else:
		slog.err("unknown os_type", os_type)
		sys.exit(1)

	interfaces = []
	slog.info(os_type)
	while True:
		line = ifconfig.readline()
		if not line:
			break

		# match interface start line
		mo = if_startline_re.search(line)
		if mo != None:
			if_name = mo.group(1)
			iface = {
				"name": if_name,
				"inet_lines": []
			}
			interfaces.append(iface)
			continue

		# match inet line
		mo = inet_line_re.search(line)
		if mo != None:
			inet = mo.group(1)
			netmask_hex = mo.group(2)
			if os_type == "freebsd":
				netmask_dec = _netmask_hex_to_dec(netmask_hex)
			elif os_type == "debian":
				netmask_dec = mo.group(2)

			iface["inet_lines"].append({})
			num_lines = len(iface["inet_lines"])
			line_dict = iface["inet_lines"][num_lines-1]

			line_dict["inet"] = inet
			line_dict["netmask_hex"] = netmask_hex
			line_dict["netmask_dec"] = netmask_dec
			continue

		# match MAC address line
		mo = ether_line_re.search(line)
		if mo != None:
			ether = mo.group(1)
			iface["ether"] = ether
			continue
	
	ifconfig.close()
	return interfaces

def default_route():
	"""Get the default gateway address."""
	netstat = os.popen("netstat -rn")

	os_type = conf.get_str("DEVICE", "os_type", "freebsd")
	# default gateway regular expression
	if os_type == "freebsd":
		default_gw_re = re.compile("^default\s+([0-9.]+)")
	elif os_type == "debian":
		default_gw_re = re.compile("^0.0.0.0\s+([0-9.]+)")
	else:
		slog.err("unknown os_type", os_type)
		sys.exit(1)
		
	default_gw = ""

	while True:
		line = netstat.readline()
		if not line:
			break

		mo = default_gw_re.search(line)
		if mo != None:
			default_gw = mo.group(1)
			break
	
	netstat.close()
	return default_gw

# nameserver regular expression
nameserver_re = re.compile("nameserver\s+([\w.]+)")

def name_servers():
	"""Read a list of nameservers from /etc/resolv.conf."""
	servers = []
	fp = file("/etc/resolv.conf")
	while True:
		line = fp.readline()
		if not line:
			break

		line = line.strip()
		if (len(line) > 0) and (line[0] == "#"):
			continue

		mo = nameserver_re.search(line)
		if mo != None:
			servers.append(mo.group(1))

	fp.close()
	return servers

def timezones():
	"""Return a list of time zone + the currently active one.
	As a tuple: ([time_zones], active_zone)
	"""
	# read all timezones from /usr/share/zoneinfo
	zones = []
	_gather_timezones("", zones)

	# /etc/localtime size
	localtime_size = os.stat("/etc/localtime")[6]

	# find the current timezone
	current_zone = ""
	for zone in zones:
		# compare zone file sizes
		zone_size = os.stat("/usr/share/zoneinfo/" + zone)[6]
		if zone_size != localtime_size:
			continue

		# compare zone file contents
		cmp_line = "cmp /usr/share/zoneinfo/" + zone + " /etc/localtime"
		compare = os.popen(cmp_line, "r")
		if compare.read().strip() == "":
			current_zone = zone
			compare.close()
			break
		compare.close()
	
	return (zones, current_zone)

def _gather_timezones(zonepath, zones):
	"""List /usr/share/zoneinfo recursively and gather timezone names."""
	zonefiles = os.listdir("/usr/share/zoneinfo/" + zonepath)
	
	for zfile in zonefiles:
		if zonepath:
			full_zone = zonepath + "/" + zfile
		else:
			full_zone = zfile
		
		if os.path.isdir("/usr/share/zoneinfo/" + full_zone):
			_gather_timezones(full_zone, zones)
			continue

		if full_zone == "zone.tab":
			continue
		
		zones.append(full_zone)

def _netmask_hex_to_dec(netmask):
	"""Convert hexadecimal netmask representation 0x$$$$$$$$ to the
	$.$.$.$ decimal type.
	"""
	if netmask[0:2] == "0x":
		netmask = netmask[2:]

	str_mask = ""
	str_mask += str(int(netmask[0:2], 16)) + "."
	str_mask += str(int(netmask[2:4], 16)) + "."
	str_mask += str(int(netmask[4:6], 16)) + "."
	str_mask += str(int(netmask[6:8], 16))

	return str_mask

def exec_cmd(cmd_line):
	"""Execute a command, return its stdout and stderr."""
	slog.info("executing", cmd_line)
	(stdin, stdout, stderr) = os.popen3(cmd_line)
	
	output = stdout.read()
	error = stderr.read()

	stdin.close()
	stdout.close()
	stderr.close()

	return (output, error)
