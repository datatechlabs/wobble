
import os, compiler
import slog, conf, modules

templates = {}		# loaded templates
template_dir = None	# template directory

def init(_template_dir):
	global template_dir, Template, Compiler

	template_dir = _template_dir
	
	try:
	        template_mod = __import__("Cheetah.Template", {}, {}, [])
		compiler_mod = __import__("Cheetah.Compiler", {}, {}, [])

		Template = template_mod.Template.Template
		Compiler = compiler_mod.Compiler.Compiler
	except ImportError:
		slog.err("cannot import Cheetah template library")
		slog.err("please check if it has been installed")
		raise

	compile_all()

def set_language(langcode):
	pass

def get_language(langcode=None):
	return "en"

def load(name):
	"""Load a template module and return the template object inside."""
	lan = get_language()
	fullname = lan + "_" + name

	# check if template class is already imported
	if fullname in templates:
		t_class = templates[fullname]
		return t_class()

	# import template module
	t_module = modules.load(fullname, template_dir + "/compiled")
	t_class = getattr(t_module, fullname)
	templates[fullname] = t_class

	return t_class()

def reload():
	global templates

	templates = {}
	compile_all()

def compile(src_file, dst_file, searchlist, modname):
	"""Compile a template.

	src_file -- path to the .tmpl file
	dst_file -- path to the .py file
	searchlist -- dictionary for filling in template placeholders
	modname -- template module name
	"""
	slog.info("compiling template %s to file: %s" % (src_file, dst_file))
	tpl = Template(file=src_file, searchList=[searchlist])
	cpl = Compiler(source=tpl, moduleName=modname)
	cpl.compile()

	# write compiled template to file
	fp = open(dst_file, 'w')
	fp.write(str(cpl))
	fp.close()

	# compile generated template to python bytecode
	compiler.compileFile(dst_file)

def compile_all():
	"""Compile (or reload) all modified templates."""
	slog.info("compiling (reloading) templates")

	# parse translation (language) files
	langfiles = os.listdir(template_dir + "/langfiles")
	languages = []
	for filename in langfiles:
		if filename[-5:] != ".conf":
			continue
		language = filename[:-5]
		fullpath = template_dir + "/langfiles/" + filename 
		conf.read(fullpath, language)
		
		modtime = int(os.stat(fullpath).st_mtime)
		languages.append((language, modtime))

	slog.info("languages", languages)
	for language, lang_modtime in languages:
		template_files = os.listdir(template_dir)
		for tfile in template_files:
			if tfile[-5:] != ".tmpl":
				continue
			
			modname = language + '_' + tfile[:-5]
			src_file = template_dir + "/" + tfile
			dst_file = template_dir + "/compiled/" + modname + ".py"

			#
			# check file modification time
			# to see if it's been modified since last compilation
			#
			
			must_compile = False
			try:
				tmpl_modtime = int(os.stat(src_file).st_mtime)
				py_modtime = int(os.stat(dst_file).st_mtime)
			except:
				must_compile = True

			if must_compile or (tmpl_modtime > py_modtime) or \
			    (lang_modtime > py_modtime):
				searchlist = conf.get_section(tfile[:-5], language)
				if searchlist == None:
					searchlist = {}

				# compile template
				compile(src_file, dst_file, searchlist, modname)
