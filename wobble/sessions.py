"""
session interface

create()
find()
set_data()
get_data()
update()
delete()
delete_expired()
"""

import time, threading
import tools, db, web

STORAGE_DB = 1
STORAGE_MEM = 2

sessions = {}	# session data (if using STORAGE_MEM)

# session cookie name
cookie_name = "WOBBLE_SESSION_ID"

# thread-local data
td = threading.local()

class Session:
	pass

def create(user_id=None, storage=STORAGE_DB, length=50, timeout=3600,
	data=None, cookie=True, cookie_path="/"):
	"""Create a new session. Returns session ID string.

	user_id -- user identification
	storage -- storage type, either database or memory
	length -- session ID length
	timeout -- session timeout
	data -- some arbitrary python data structure
	cookie -- whether to set a cookie
	cookie_path -- cookie path
	"""
	session_id = tools.randstr(length)
	expires = int(time.time()) + timeout
	ip_address = web.getvar_env("REMOTE_ADDR")
	#userXForwardedIp = web.getvar_env('XFORWARDED_CLIENT_IP')
	
	# delete expired sessions
	delete_expired(storage)
	
	row = {
		"session_id": session_id,
		"user_id": user_id,
		"ip_address": ip_address,
		"exp_timestamp": expires,
		"timeout": timeout,
		"data": data
	}

	if storage == STORAGE_DB:
		db.execute("create_session", row)
	elif storage == STORAGE_MEM:
		sessions[session_id] = row
	else:
		raise Exception("Invalid storage type")
	
	# store session id in cookie
	if cookie:
		web.setcookie(name=cookie_name, value=session_id, path=cookie_path)
	
	return session_id
	
def find(session_id=None, storage=STORAGE_DB):
	"""Perform lookup in session database and returns session id if found.

	session_id -- session ID string
	storage -- storage type, either database or memory
	"""
	if session_id == None:
		# get session id from cookie
		session_id = web.getvar_cookie(cookie_name, None)
		if session_id == None:
			return None

	if hasattr(td, session_id):
		return getattr(td, session_id)

	if storage == STORAGE_DB:
		row = {"session_id": session_id}
		session = db.execute("find_session", row).fetchone()
	elif storage == STORAGE_MEM:
		session = sessions.get(session_id)
	else:
		raise Exception("Invalid storage type")

	if session == None:
		return None

	if storage == STORAGE_MEM:
		# convert session dictionary to a Session object
		s = Session()
		for k, v in session.iteritems():
			setattr(s, k, v)
		session = s

	# check if session has expired
	if time.time() >= session.exp_timestamp:
		return None
		
	# check if user ip address is still the same
	if session.ip_address != web.getvar_env("REMOTE_ADDR"):
		return None

	# store session data in thread data for caching reasons
	setattr(td, session_id, session)
	
	# return session data
	return session

def set_data(session_id=None):
	pass

def get_data(session_id=None):
	pass

def update(session_id=None, storage=STORAGE_DB, data=None):
	"""Reset session exp time making it valid for longer time."""
	
	session = find(session_id)
	if session == None:
		return

	row = {
		"session_id": session_id,
		"exp_timestamp": int(time.time()) + session.timeout,
		"data": data
	}

	# update session by setting its expiry time to current time + timeout
	if storage == STORAGE_DB:
		db.execute("update_session", row)
	elif storage == STORAGE_MEM:
		if session_id in sessions:
			sessions[session_id].update(row)
	else:
		raise Exception("Invalid storage type")

def delete(session_id=None, storage=STORAGE_DB, cookie=True, cookie_path="/"):
	"""Delete session."""
	
	if session_id == None:
		session_id = web.getvar(cookie_name)
		if session_id == None:
			return

	if storage == STORAGE_DB:
		row = {"session_id": session_id}
		db.execute("delete_session", row)
	elif storage == STORAGE_MEM:
		sessions.pop(session_id, None)
	else:
		raise Exception("Invalid storage type")

	# remove cookie
	if cookie:
		web.delcookie(cookie_name, path=cookie_path)

def delete_expired(storage=STORAGE_DB):
	"""Delete expired sessions."""
	
	if storage == STORAGE_DB:
		params = {"now": int(time.time())}
		db.execute("delete_expired_sessions", params)
	elif storage == STORAGE_MEM:
		for session_id in sessions.keys():
			if time.time() >= sessions[session_id]["exp_timestamp"]:
				del sessions[session_id]
	else:
		raise Exception("Invalid storage type")
