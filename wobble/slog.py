"""
Stream logging module -- logs messages to an output stream (stderr by default)
preceded by a tag (e.g., 'WARNING', 'ERROR'). A tag can be any arbitrary
string. To set which tagged messages will be written to the output stream, use
the function output_tags().

For actually writing something to the output stream call write(). Several
shortcut functions are provided which use predefined tags: info(), warn(),
err(), verb(), xverb().

Set the output stream with set_stream().
"""

import sys, time, os, threading
from pprint import pprint

log_tags = ["INFO", "WARNING", "ERROR"]

logstream = sys.stderr
write_lock = threading.Lock()

def set_stream(fp):
	global logstream

	if isinstance(fp, str):
		logstream = open(fp, 'a')
	else:
		logstream = fp

	# make program errors go to our log stream
	sys.stderr = logstream

	# make stdout go to our log stream
	# sys.stdout = logstream
	
		
def rotate_stream(name, newname):

	global logstream
	sys.stderr = sys.__stderr__
	info("Rotating ", name, "to", newname)

	write_lock.acquire()

	logstream.close()

	try:
		os.rename(name, newname)
	except:
		return False
	set_stream(name)
	write_lock.release()
	info("Rotation successful")
	return True


def output_tags(tags):
	global log_tags

	log_tags = []
	for tag in tags:
		log_tags.append(tag.upper())
	info("Logging tags:", log_tags)

def write(tag, *msg):
	if tag not in log_tags:
		return

	if (tag == "INFO") or (tag == "VERBOSE"):
		tag = ""
	else:
		tag = tag + ": "

	tstamp = time.time()
	now = time.localtime(tstamp)
	usec = int((tstamp - int(tstamp)) * 1000000)
	timestamp = time.strftime("%Y-%m-%d %H:%M:%S", now) + ".%06d" % (usec)

	write_lock.acquire()

	logstream.write(timestamp + ' ' + tag)
	for x in msg:
		if isinstance(x, str) or isinstance(x, int) or \
		    isinstance(x, list) or isinstance(x, tuple) or \
		    isinstance(x, long) or isinstance(x, float) or \
		    isinstance(x, unicode) or (x == None):
			logstream.write(str(x) + " ")
		else:
			logstream.write("\n")
			pprint(x, logstream)
	logstream.write("\n")
	logstream.flush()

	write_lock.release()

def info(*msg):
	"""Info message."""
	
	write("INFO", *msg)

debug = info

def warn(*msg):
	"""Warning message."""
	
	write("WARNING", *msg)

def err(*msg):
	"""Error message."""
	
	write("ERROR", *msg)

def verb(*msg):
	"""Verbose message for serious debugging."""
	
	write("VERBOSE", *msg)

def xverb(*msg):
	"""EXTREMELY verbose message. Avoid this."""
	
	write("XVERBOSE", *msg)

def db(*msg):
	"""database debugging."""
	
	write("DB", *msg)
