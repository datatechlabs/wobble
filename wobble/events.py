"""
The purpose of this module is to notify users/administrators about critical
situations during the operation of a program.

So far there are three ways of doing this (not all of them implemented yet):
	* logging information about an important event into database which can
	  later be viewed from the user interface
	* sending an email to somebody who's interested
	* sending SMS messages to cell phones
"""

import slog
import db as dbmod

source = None

def set_source(name):
	global source
	source = name

def db(evtype, level, message, data=None):
	"""
	Logs an event into a database.

	evtype -- event type: 'database', 'network', 'billing-auth', etc
	level -- 'error', 'notice', 'warning', etc
	message -- descriptive message
	data -- additional data concerning the event
	"""
	if data == None:
		data = ""
	
	row = {
		"message": message,
		"data": str(data),
		"level": level,
		"type": evtype,
		"source": source
	}
	dbmod.execute("log_event", row)

def sms():
	pass

def mail():
	pass
