
from common_if import *

import socket, cgi, traceback, re
from threading import Thread
from wobble import slog

user_callback = None
serv_sock = None
serv_addr = None

# regexp for getting boundary from Content-Type header
boundary_regexp = re.compile("boundary=(.+)")

REQUEST_QUEUE_SIZE = 5

http_codes = {
	100: ('Continue', 'Request received, please continue'),
	101: ('Switching Protocols',
	    'Switching to new protocol; obey Upgrade header'),

	200: ('OK', 'Request fulfilled, document follows'),
	201: ('Created', 'Document created, URL follows'),
	202: ('Accepted',
	    'Request accepted, processing continues off-line'),
	203: ('Non-Authoritative Information', 'Request fulfilled from cache'),
	204: ('No Content', 'Request fulfilled, nothing follows'),
	205: ('Reset Content', 'Clear input form for further input.'),
	206: ('Partial Content', 'Partial content follows.'),

	300: ('Multiple Choices',
	    'Object has several resources -- see URI list'),
	301: ('Moved Permanently', 'Object moved permanently -- see URI list'),
	302: ('Found', 'Object moved temporarily -- see URI list'),
	303: ('See Other', 'Object moved -- see Method and URL list'),
	304: ('Not Modified', 'Document has not changed since given time'),
	305: ('Use Proxy',
	    'You must use proxy specified in Location to access this '
	    ' resource.'),
	307: ('Temporary Redirect',
	    'Object moved temporarily -- see URI list'),
	400: ('Bad Request', 'Bad request syntax or unsupported method'),
	401: ('Unauthorized', 'No permission -- see authorization schemes'),
	402: ('Payment Required', 'No payment -- see charging schemes'),
	403: ('Forbidden', 'Request forbidden -- authorization will not help'),
	404: ('Not Found', 'Nothing matches the given URI'),
	405: ('Method Not Allowed',
	    'Specified method is invalid for this server.'),
	406: ('Not Acceptable', 'URI not available in preferred format.'),
	407: ('Proxy Authentication Required', 'You must authenticate with '
	    'this proxy before proceeding.'),
	408: ('Request Timeout', 'Request timed out; try again later.'),
	409: ('Conflict', 'Request conflict.'),
	410: ('Gone',
	    'URI no longer exists and has been permanently removed.'),
	411: ('Length Required', 'Client must specify Content-Length.'),
	412: ('Precondition Failed', 'Precondition in headers is false.'),
	413: ('Request Entity Too Large', 'Entity is too large.'),
	414: ('Request-URI Too Long', 'URI is too long.'),
	415: ('Unsupported Media Type', 'Entity body in unsupported format.'),
	416: ('Requested Range Not Satisfiable',
	    'Cannot satisfy request range.'),
	417: ('Expectation Failed',
	    'Expect condition could not be satisfied.'),
	500: ('Internal Server Error', 'Server got itself in trouble'),
	501: ('Not Implemented', 'Server does not support this operation'),
	502: ('Bad Gateway', 'Invalid responses from another server/proxy.'),
	503: ('Service Unavailable',
	    'The server cannot process the request due to a high load'),
	504: ('Gateway Timeout',
	    'The gateway server did not receive a timely response'),
	505: ('HTTP Version Not Supported', 'Cannot fulfill request.')
}

weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
monthname = [None, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
    'Sep', 'Oct', 'Nov', 'Dec']

def _timestr():
        """Return the current date and time formatted for a message header."""
	
	timestamp = time.time()
        year, month, day, hh, mm, ss, wd, y, z = time.gmtime(timestamp)
        s = "%s, %02d %3s %4d %02d:%02d:%02d GMT" % (
	    weekdayname[wd], day, monthname[month], year, hh, mm, ss)
        return s

def _parse_request(sock, addr):
	"""Parse HTTP request."""

	global serv_addr, user_callback, boundary_regexp

	td.sock = sock
	td.user_control = False
	td.headers = []
	td.response = (200, "OK")
	td.content = ""

	# set environment
	env = {}
	env["SERVER_SOFTWARE"] = "wobble builtin 0.1"
	td.env = env
	
	rfile = sock.makefile("r")

	req_line = rfile.readline()
	if req_line == "":
		slog.err("Empty request.")
		_send_response(0)
		return

	# cut off CRLF or LF
	if req_line[-2:] == "\r\n":
		req_line = req_line[:-2]
	else:
		req_line = req_line[:-1]

	words = req_line.split()
	if len(words) != 3:
		slog.err("Request does not contain three words:", words)
		_send_response(400)
		return

	(command, path, protocol) = words
	if protocol[:5] != "HTTP/":
		slog.err("Unrecognized protocol:", protocol)
		_send_response(400)
		return

	if (command != "GET") and (command != "POST"):
		slog.err("Unsupported method:", command)
		_send_response(400)
		return

	slog.xverb("HTTP request:", req_line)

	# Some simple (or lame, rather) header parsing.
	# They are stored in the thread context environment
	req_headers = {}
	while True:
		hline = rfile.readline()
		if hline == "":
			break
		
		hline = hline.strip()
		if hline == "":
			# end of headers
			break
		
		pair = hline.split(':', 1)
		if len(pair) != 2:
			slog.warn("Unrecognized header line:", hline)
			continue

		req_headers[pair[0]] = pair[1].strip()

	# store request headers
	td.req_headers = req_headers

	# get query string
	query_string = ""
	pos = path.find('?')
	if pos != -1:
		query_string = path[pos+1:]
		
	# store GET variables
	td.getvars = cgi.parse_qs(query_string, True)

	slog.xverb("HTTP headers:", req_headers)

	# prepare environment
	env["REQUEST_METHOD"] = command
	env["REQUEST_URI"] = path
	env["PATH_INFO"] = path
	env["SCRIPT_NAME"] = ""
	env["QUERY_STRING"] = ""
	env["CONTENT_TYPE"] = req_headers.get("Content-Type", "")
	env["CONTENT_LENGTH"] = req_headers.get("Content-Length", "")
	env["HTTP_COOKIE"] = req_headers.get("Cookie", "")
	env["REMOTE_ADDR"] = addr[0]
	env["SERVER_ADDR"] = serv_addr[0]
	env["SERVER_PORT"] = str(serv_addr[1])
	env["QUERY_STRING"] = query_string

	# set server name
	host_header = req_headers.get("Host", "")
	if host_header != "":
		env["SERVER_NAME"] = host_header
	else:
		env["SERVER_NAME"] = serv_addr[0] + ":" + str(serv_addr[1])

	# store POST
	td.postvars = {}
	if env["CONTENT_TYPE"].find("multipart/form-data") != -1:
		# XXX: this is incomplete and will probably break
		# in certain situations (but do we care?)
		mo = boundary_regexp.search(env["CONTENT_TYPE"])
		if mo != None:
			pdict = {"boundary": mo.group(1)}
			td.postvars = cgi.parse_multipart(rfile, pdict)
	else:
		td.postvars = cgi.parse(rfile, env, 1)

	slog.xverb("HTTP POST variables:", td.postvars)

	# store cookies
	cookie_obj = Cookie.SimpleCookie()
	cookie_obj.load(env["HTTP_COOKIE"])
	cookies = {}
	for key, morsel in cookie_obj.iteritems():
		cookies[key] = morsel.value
	td.cookies = cookies

	user_callback()

	# check if user controlled the output herself
	if td.user_control:
		sock.close()
		return
	
	code, descr = td.response

	_send_response(code, descr)
	
def _send_response(code, descr=""):
	"""Send response back to the client.

	code -- HTTP response code
	descr -- human readable description
	"""
	global http_codes

	sock = td.sock
	
	if code == 0:
		sock.close()
		return

	# main response line
	if descr == "":
		descr = http_codes[code][0]
	r = "%s %d %s\r\n" % ("HTTP/1.0", code, descr)

	header("Server", td.env["SERVER_SOFTWARE"])
	header("Date", _timestr())
	header("Connection", "close")

	sock.sendall(r)
	sock.sendall(_header_str())
	sock.sendall(td.content)
	sock.close()

def output_force_start(content):
	"""Force sending of response line and headers (+initial content)."""
	
	sock = td.sock
	td.user_control = True

	r = "%s %d %s\r\n" % ("HTTP/1.1", 200, "OK Forced")

	header("Server", td.env["SERVER_SOFTWARE"])
	header("Date", _timestr())
	header("Connection", "close")

	sock.sendall(r)
	sock.sendall(_header_str())
	sock.sendall(content)

def output_force(content):
	"""Force output."""
	
	sock = td.sock
	sock.sendall(content)

def _header_str():
	"""Compose and return complete response."""

	r = ""
	for h in td.headers:
		r += ("%s: %s\r\n" % h)
	r += "\r\n"

	return r

def init(user_func, ip_addr, port):
	"""Initialize simple server."""

	global user_callback, serv_sock, serv_addr

	user_callback = user_func
	serv_addr = (ip_addr, port)

	serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	serv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	serv_sock.bind((ip_addr, port))
	serv_sock.listen(REQUEST_QUEUE_SIZE)

def run():
	"""Listen and respond to requests."""

	while True:
		(sock, addr) = serv_sock.accept()
		th = Thread(target=_parse_request, args=(sock, addr))
		th.start()
