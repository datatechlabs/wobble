"""
SCGI interface support
"""

from common_if import *
from scgi import scgi_server

# user main function
user_func = None

# server instance
server_instance = None

class MyHandler(scgi_server.SCGIHandler):
	"""Wraps user function to work with flup SCGI."""
	
	def produce(self, environ, bodysize, input, output):
		# setup thread storage
		td.headers = []
		td.response = (200, "OK")
		td.content = ""

		# get and parse query string
		query_string = environ.get("QUERY_STRING", "")
		td.getvars = cgi.parse_qs(query_string, True)

		# store POST
		input_file = input
		environ["QUERY_STRING"] = ""
		td.postvars = cgi.parse(input_file, environ, 1)

		# store cookies
		cookie_obj = Cookie.SimpleCookie()
		cookie_obj.load(environ.get("HTTP_COOKIE", ""))
		cookies = {}
		for key, morsel in cookie_obj.iteritems():
			cookies[key] = morsel.value
		td.cookies = cookies
		
		# store environment
		environ["QUERY_STRING"] = query_string
		td.env = environ
	
		# execute user function
		user_func()

		# output response and headers
		code, descr = td.response
		header("Content-Length", str(len(td.content)))

		output.write("Status: " + str(code) + ' ' + descr + "\r\n")
		for k, v in td.headers:
			output.write(k + ": " + v + "\r\n")
		output.write("\r\n")

		output.write(td.content)

#
# Below are functions callable by user
#

def init(_user_func, ip="127.0.0.1", port=9776):
	"""Initialize web module."""
	global user_func, server_instance

	user_func = _user_func
	server_instance = scgi_server.SCGIServer(
#	    max_children=1,
	    host=ip,
	    port=port,
	    handler_class=MyHandler)
	
def run():
	"""Listen to requests"""
	return server_instance.serve()
