"""
Common webserver interface
"""

import cgi, time, Cookie, threading, json

# thread-local storage
td = threading.local()

def response(code, descr):
	"""Set HTTP response."""
	td.response = (code, descr)

def header(key, value):
	"""Append html header."""
	td.headers.append((key, value))

def output(data):
	"""Output page content."""
	content = td.content
	td.content += str(data)

def getvar_get(name, default=None):
	"""Get the value of a GET variable."""
	getvars = td.getvars
	if getvars.has_key(name):
		return getvars[name][0]
	return default

def getvar_post(name, default=None):
	"""Get the value of a POST variable."""
	postvars = td.postvars
	if postvars.has_key(name):
		return postvars[name][0]
	return default

def getvar_json():
	"""Get the JSON from POST and convert to object"""
	postvars = td.postvars
	try:
		k = postvars.keys()[0]
	except:
		return {}
	try:
		vars = json.loads(k)
	except:
		vars = {}
	return vars

def getvar_cookie(name, default=None):
	"""Get cookie value."""
	cookies = td.cookies
	return cookies.get(name, default)

def getvar_env(name, default=None):
	"""Get environment variable."""
	env = td.env
	return env.get(name, default)

def setcookie(name, value, expire=None, path = '/', domain=None):
	"""Send a Set-Cookie header."""
	if value == None:
		value = ""

	cookie = Cookie.SimpleCookie()
	cookie[name] = value

	if expire != None:
		expire = time.gmtime(expire)
		expdate = time.strftime("%A, %d-%b-%Y %H-%M-%S GMT", expire)
		cookie[name]["expires"] = expdate
		
	if domain != None:
		cookie[name]["domain"] = domain

	cookie[name]["path"] = path

	header("Set-Cookie", cookie[name].OutputString())

def delcookie(name, expire=None, path = '/', domain=None):
	"""Remove cookie."""
	setcookie(name, None, expire, path, domain)

def getvar(name, default=None):
	"""Get variable from GET, POST or environment."""
	value = getvar_get(name, default)
	if value != default:
		return value

	value = getvar_post(name, default)
	if value != default:
		return value

	value = getvar_cookie(name, default)
	if value != default:
		return value

	return getvar_env(name, default)

def redirect(location):
	"""HTTP Redirect."""
	servname = getvar_env("SERVER_NAME")
	response(302, "Redirect")
	header("Location", "http://" + servname + location)
	
def set_default_headers(headers=[("Content-Type", "text/html")]):
	"""Set default HTTP headers if neccessary."""
	
	new_headers = []
	for arg_key, arg_value in headers:
		found = False
		for key, value in td.headers:
			if key.lower() == arg_key.lower():
				found = True
				break
		if not found:
			new_headers.append((arg_key, arg_value))

	td.headers += new_headers

def rm_output():
	"""Clear all output."""
	
	td.content = ""

#
# display various errors to user
#

def error_not_found():
	response(404, "Not Found")
	header("Content-Type", "text/html")
	output('<h1>404 - Not Found</h1>')

def error_internal():
	response(500, "Internal Server Error")
	header("Content-Type", "text/html")
	output("<h1>500 - Internal Server Error</h1>")
