"""
Defines CGI interface.
I don't suggest to realy on this module because it has never
been actually tested or used.
"""

# HeadURL		$HeadURL: svn://alpha:33033/webstuff/trunk/webstuff/server/cgi_if.py $
# Author:		$Author: atis $
# File version:	$Revision: 2078 $
# Last changes:	$Date: 2006-07-17 16:44:00 +0300 (Mon, 17 Jul 2006) $


from common_if import *
import os, cgi, sys

user_func = None

def init(_user_func, ip = '', port = 0):
	"""Store user function.
		Ip and port attributes are just for compatibility with
		other server interface modules. They are not really
		used anywhere.
	"""
	# store user function
	global user_func
	user_func = _user_func
	
def run():
	td.headers = []
	td.response = (200, "OK")
	td.content = ""
		
	# retrieve GET variables
	getvars = {}
	if os.environ.has_key("QUERY_STRING"):
		getvars = cgi.parse_qs(os.environ["QUERY_STRING"], True)
	td.getvars = getvars

	# retrieve POST variables
	td.postvars = cgi.FieldStorage()

	# store cookies
	cookie_obj = Cookie.SimpleCookie()
	cookie_obj.load(os.environ.get("HTTP_COOKIE", ""))
	cookies = {}
	for key, morsel in cookie_obj.iteritems():
		cookies[key] = morsel.value
	td.cookies = cookies
		
	# execute user function
	user_func()

	# send status header
	response = td.response
	print "Status: %s %s" % response
	
	# print headers
	header("Content-Length", len(td.content))
	for key, value in td.headers:
		print "%s: %s" % (key, value)
	print "\n",
	print td.content
	return None
