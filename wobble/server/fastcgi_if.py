"""
Defines FCGI server
"""

from common_if import *
import flup.server.fcgi as fcgi

# user main function
user_func = None

# server instance
server_instance = None

def _main(environ, start_response):
	"""Wraps user function to work with flup FastCGI."""

	# setup thread storage
	td.headers = []
	td.response = (200, "OK")
	td.content = ""

	# get and parse query string
	query_string = environ.get("QUERY_STRING", "")
	td.getvars = cgi.parse_qs(query_string, True)

	# store POST
	input_file = environ["wsgi.input"]
	environ["QUERY_STRING"] = ""
	td.postvars = cgi.parse(input_file, environ, 1)

	# store cookies
	cookie_obj = Cookie.SimpleCookie()
	cookie_obj.load(environ.get("HTTP_COOKIE", ""))
	cookies = {}
	for key, morsel in cookie_obj.iteritems():
		cookies[key] = morsel.value
	td.cookies = cookies
		
	# store environment
	environ["QUERY_STRING"] = query_string
	td.env = environ
	
	# execute user function
	user_func()

	# output response and headers
	code, descr = td.response
	header("Content-Length", str(len(td.content)))
	start_response(str(code) + ' ' + descr, td.headers)
	
	# get content
	content = td.content

	return [content]

#
# Below are functions callable by user
#

def init(_user_func, ip="127.0.0.1", port=9777):
	"""Initialize web module."""
	global user_func, server_instance

	user_func = _user_func
	addr = (ip, port)
	server_instance = fcgi.WSGIServer(_main, bindAddress=addr)
	
def run():
	"""Listen to requests"""
	return server_instance.run()
