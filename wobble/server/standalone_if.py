"""
Defines standalone webserver
"""

from common_if import *

import re, SocketServer
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from wobble import slog

# server instance
serverInstance = None

# regexp for getting boundary from Content-Type header
boundary_regexp = re.compile("boundary=(.+)")

class MyHandler(BaseHTTPRequestHandler):
	def proc(self):
		# setup thread storage
		td.headers = []
		td.response = (200, "OK")
		td.content = ""
		
		# get query string
		query_string = ""
		pos = self.path.find('?')
		if pos != -1:
			query_string = self.path[pos+1:]
			
		# store GET variables
		td.getvars = cgi.parse_qs(query_string, True)
		
		# set up environment
		env = {}
		env["REQUEST_METHOD"] = self.command
		env["REQUEST_URI"] = self.path
		env["PATH_INFO"] = self.path
		env["SCRIPT_NAME"] = ""
		env["QUERY_STRING"] = ""
		env["CONTENT_TYPE"] = self.headers.get("Content-Type", "")
		env["CONTENT_LENGTH"] = self.headers.get("Content-Length", "")
		env["HTTP_COOKIE"] = self.headers.get("Cookie", "")
		env["REMOTE_ADDR"] = self.client_address[0]
		env["SERVER_ADDR"] = self.server.server_address[0]
		env["SERVER_PORT"] = str(self.server.server_address[1])
		env["SERVER_PROTOCOL"] = self.request_version
		env["SERVER_SOFTWARE"] = "BaseHTTPServer"
		
		addr = env["SERVER_ADDR"]
		port = str(env["SERVER_PORT"])
		host_header = self.headers.get("Host", "")
		if host_header != "":
			env["SERVER_NAME"] = host_header
		else:
			env["SERVER_NAME"] = addr + ":" + port

		# store POST
		td.postvars = {}
		if env["CONTENT_TYPE"].find("multipart/form-data") != -1:
			# XXX: this is incomplete and will probably break
			# in certain situations
			mo = boundary_regexp.search(env["CONTENT_TYPE"])
			if mo != None:
				pdict = {"boundary": mo.group(1)}
				td.postvars = cgi.parse_multipart(self.rfile, pdict)
		else:
			td.postvars = cgi.parse(self.rfile, env, 1)

		self.rfile.close()
		
		# store cookies
		cookie_obj = Cookie.SimpleCookie()
		cookie_obj.load(env["HTTP_COOKIE"])
		cookies = {}
		for key, morsel in cookie_obj.iteritems():
			cookies[key] = morsel.value
		td.cookies = cookies

		# store environment
		env["QUERY_STRING"] = query_string
		td.env = env
		
		# execute user function
		self.server.user_func()
			
		# write response, headers, a blank line and the output
		code, descr = td.response
		self.send_response(code, descr)
		
		header("Content-Length", len(td.content))
		for key, value in td.headers:
			self.send_header(key, value)
		self.wfile.write("\r\n")
			
		self.wfile.write(td.content)
	
	def do_GET(self):
		self.proc()

	def do_POST(self):
		self.proc()

	def log_message(self, format, *args):
		slog.info(
			self.address_string(),
			self.log_date_time_string(),
			format % args
		)

	def address_string(self):
		"""Return the client address formatted for logging.

		This is an overridden version that omits socket.getfqdn()
		since that tends to hang the server when DNS is not
		available
		"""
		host, port = self.client_address[:2]
		return host

class MyServer(HTTPServer):
	def __init__(self, addr, user_func):
		HTTPServer.__init__(self, addr, MyHandler)
		self.user_func = user_func

		# set our own logging function
		BaseHTTPRequestHandler.log_message = MyHandler.log_message

	def server_bind(self):
		"""Override server_bind to store the server name."""
		SocketServer.TCPServer.server_bind(self)
		host, port = self.socket.getsockname()[:2]
		self.server_name = host
		self.server_port = port

#
# Below are functions callable by user
#

def init(user_func, ip="127.0.0.1", port=8000):
	"""Initialize standalone HTTP server."""
	addr = (ip, port)
	global serverInstance
	serverInstance = MyServer(addr, user_func)
	
def run():
	"""Listen & respond to requests"""
	return serverInstance.serve_forever()
