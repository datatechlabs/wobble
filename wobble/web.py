
import re, sys
import slog, modules

def set_interface(server_type):
	"""Import web interface module and make it accessible globally.

	server_type -- the type of server you wish to run ('standalone',
		'fastcgi', 'cgi')
	"""
	if server_type == "builtin":
		import wobble.server.builtin_if as web_module
	elif server_type == "standalone":
		import wobble.server.standalone_if as web_module
	elif server_type == "fastcgi":
		import wobble.server.fastcgi_if as web_module
	elif server_type == "scgi":
		import wobble.server.scgi_if as web_module
	else:
		slog.err("unknown server type", server_type)
		sys.exit(1)

	# make all contents of the imported web interface as part of this
	# module (accessible globally)
	web = globals()
	for attr in dir(web_module):
		if attr.startswith("__"):
			continue

		web[attr] = getattr(web_module, attr)

def parse_urlmap(urlmap, module_dir):
	"""Parse url regexp mapping.

	urlmap -- dictionary in the form (url_regexp: 'module.function')
	module_dir -- directory where modules should be located

	Returns a parsed url mapping where url regexps have been compiled and
	function names replaced with references to those functions. The
	returned dictionary has the form (compiled_regexp, module_name,
	function_reference)
	"""
	slog.info("parsing url mapping")

	parsed_urlmap = []
	
	for url_regexp, modfunc in urlmap:
		(module_name, function_name) = modfunc.split('.')
		url_regexp = re.compile(url_regexp)
		module = modules.load(module_name, module_dir, "usermod")
		function = getattr(module, function_name)
		
		# store the compiled regexp + function reference
		parsed_urlmap.append((url_regexp, module_name, function))

	return parsed_urlmap
