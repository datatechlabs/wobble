"""
Configuration file parser.
"""

import sys, os, re

values = {}

def read(filename, alias="noalias", type="ini", try_defaults=True):
	"""Read and parse a configuration file.

	filename -- path of the configuration file
	alias -- the name used for referring to the parsed configuration
	type -- configuration file type ("ini", "php")
	try_defaults -- whether to look for a .defaults configuration file
	"""
	global values

	if alias not in values:
		values[alias] = {}

	got_conf = False	# whether we managed to read some configuration

	if try_defaults:
		# read default configuration (if it exists)
		def_filename = filename + ".defaults"
		if os.access(def_filename, os.F_OK):
			# read configuration and store it
			if type == "ini":
				conf = _read_ini_conf(def_filename)
				_update_conf_dict(values[alias], conf)
			elif type == "php":
				conf = _read_php_conf(def_filename)
				if "php" not in values[alias]:
					values[alias]["php"] = {}
				values[alias]["php"].update(conf)
			else:
				raise Exception("unknown config type: " + type)

			got_conf = True

	# read configuration
	if os.access(filename, os.F_OK):
		if type == "ini":
			conf = _read_ini_conf(filename)
			_update_conf_dict(values[alias], conf)
		elif type == "php":
			conf = _read_php_conf(filename)
			if "php" not in values[alias]:
				values[alias]["php"] = {}
			values[alias]["php"].update(conf)
		else:
			raise Exception("unknown config type: " + type)

		got_conf = True

	if not got_conf:
		raise Exception("could not read any configuration files")

def get_str(section, option, default=None, alias="noalias"):
	"""Get a string value from the configuration."""
	try:
		value = values[alias][section][option]
	except:
		if default != None:
			return default
		raise
	return str(value)

def get_int(section, option, default=None, alias="noalias"):
	"""Get an integer value from the configuration."""
	try:
		value = values[alias][section][option]
	except:
		if default != None:
			return default
		raise

	if isinstance(value, int):
		return value

	values[alias][section][option] = int(value)
	return int(value)

def get_float(section, option, default=None, alias="noalias"):
	"""Get a floating point value from the configuration."""
	try:
		value = values[alias][section][option]
	except:
		if default != None:
			return default
		raise

	if isinstance(value, float):
		return value

	values[alias][section][option] = float(value)
	return float(value)

def get_bool(section, option, default=None, alias="noalias"):
	"""Get a boolean value."""
	try:
		value = values[alias][section][option]
	except:
		if default != None:
			return default
		raise

	if isinstance(value, bool):
		return value

	# if it's not a string, just check what it evaluates to
	if (not isinstance(value, str)) and (not isinstance(value, unicode)):
		if value:
			values[alias][section][option] = True
			return True
		
		values[alias][section][option] = False
		return False

	# handle strings
	# we ignore the strings "on"/"off" since "on" is very
	# much like "no" and nasty typos could occur
	if (value == "0") or (value == "false") or (value == "no"):
		values[alias][section][option] = False
		return False
	
	values[alias][section][option] = True
	return True

def get_list(section, option, sep=',', default=None, alias="noalias"):
	"""Get a value separated list"""
	try:
		value = values[alias][section][option]
	except KeyError:
		if default != None:
			return default
		raise

	if isinstance(value, list):
		return value

	# break up the list string
	list_str = get_str(section, option, default, alias)
	raw_list = list_str.split(sep)

	final_list = []
	for listval in raw_list:
		if listval.strip():
			final_list.append(listval.strip())

	# store as a list for later retrieval
	values[alias][section][option] = final_list
	return final_list

def set_value(section, option, value, alias="noalias"):
	"""Set an option."""
	if alias not in values:
		values[alias] = {}
	if section not in values[alias]:
		values[alias][section] = {}
	values[alias][section][option] = value

def del_value(section, option, alias="noalias"):
	"""Delete an option."""
	try:
		del values[alias][section][option]
	except:
		pass

def get_section(name, alias="noalias"):
	try:
		section = values[alias][name]
		return section
	except:
		pass

	return None

def get_config(alias="noalias"):
	try:
		config = values[alias]
		return config
	except:
		pass

	return None

def del_config(alias="noalias"):
	try:
		del values[alias]
	except:
		pass

def get_all():
	return values

def _update_conf_dict(orig, bonus):
	for section, values in bonus.iteritems():
		if section not in orig:
			orig[section] = {}
		for key, value in values.iteritems():
			orig[section][key] = value

def _read_ini_conf(filename):
	"""Reads .ini-like configuration files"""
	config = {"UNNAMED": {}}
	section = "UNNAMED"
	fp = open(filename, 'r')
	
	while True:
		line = fp.readline()
		if (not line):
			break

		# check if it's an empty line or a comment
		line = line.strip()
		if (not line or line[0] == '#' or line[0] == ';'):
			continue

		pair = line.split('=', 1)
		if (len(pair) == 1):
			# a new section
			section = pair[0]
			if (len(section) < 3 or section[0] != '[' or \
			    section[-1] != ']'):
				break

			section = section[1:-1]
			if (not (section in config.keys())):
				config[section] = {}
			continue

		key = pair[0].rstrip()
		value = pair[1].lstrip()

		if (len(value) > 1 and value[0] == '"'):
			value = value[1:-1]

		matches = re.findall(r"%\((\w+)\)", value)
		# replace %(xxx) patterns
		for pat in matches:
			meta = '%(' + pat + ')'
			repl = config[section][pat]
			value = value.replace(meta, repl)

		config[section][key] = value

	fp.close()
	return config

#
# PHP config file parsing
#

# represents multiline php comments: /*.....*/
_multilinePhpCommentRegexp = re.compile(r'/\*(.*?)\*/', re.DOTALL)

# represents single line php comments: //....
_singleLinePhpCommentRegexp = re.compile(r'//.*\n')

# matches whitespace characters at the beginning and end of line in multiline string
_stripRegexp = re.compile(r'^\s*|\s*\n', re.MULTILINE)

# matches php statement: $.....;
_phpStatementRegexp = re.compile(r'\$(.*?);', re.DOTALL)

# matches one single or double quote at the beginning or end of string
_quoteStripRegexp = re.compile(r'^\s*[\'"]?|[\'"]?\s*$')

def _read_php_conf(filename):
	"""Parse config file which actually is simple php file.	Raises
		exception on error.
		Input: (file) file like object
		Output: none
	"""
	config = {}
		
	# Read all the file into the memory.
	# It is needed to cut out php's bloody multiline comments.
	fileContents = file(filename).read()
	
	# cut multiline comments
	fileContents = _multilinePhpCommentRegexp.sub('', fileContents)
	fileContents = _singleLinePhpCommentRegexp.sub('', fileContents)
	fileContents = _stripRegexp.sub('', fileContents)
	phpStatements = _phpStatementRegexp.findall(fileContents)
		
	# read line by line
	for statement in phpStatements:
		# split statements by assignment operator
		tokens = statement.split('=', 1)
		if len(tokens) != 2:
			continue
		# get varible name and value without surrounding whitespace and quotes
		var = _quoteStripRegexp.sub('', tokens[0])
		value = _quoteStripRegexp.sub('', tokens[1])
			
		config[var] = value

	return config
