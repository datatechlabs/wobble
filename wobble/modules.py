
import os, imp, time
import slog

module_map = {}	# loaded modules

def load(name, directory, group="default", reload=False, store=True):
	"""Load a python module.

	name -- module name
	directory -- path to module directory
	group -- module group (so that it'd be possible to relaod only
		a certain group of modules rather than all of them)
	store -- whether to store it in a global dictionary so that
		later it can be automatically reloaded

	Returns a reference to the module.
	"""
	try:
		fp, path, descr = imp.find_module(name, [directory])
	except ImportError:
		slog.err("Cannot find module", name, "in directory", directory)
		raise

	# check if module has already been loaded
	if reload == False:
		if (group in module_map) and (path in module_map[group]):
			fp.close()
			return module_map[group][path][0]

	slog.info("Loading module", name, "from directory", directory,
		"into group", group)
	module = imp.load_module(name, fp, path, descr)
	fp.close()

	# store the module reference, the time it was loaded
	# and other useful info
	if group not in module_map:
		module_map[group] = {}
	module_map[group][path] = (module, name, directory, int(time.time()))
	
	return module

def reload(group=None):
	"""Reload modules that have changed on disk.

	group -- name of the module group to reload, pass None to reload all
		modules that were loaded with load_module()
	"""
	if group != None:
		# reload the modified modules within the selected group
		slog.info("Reloading modules from group", group)
		group = module_map[group]
		for (modpath, modattr) in group.iteritems():
			# extract module attributes
			name = modattr[1]
			directory = modattr[2]
			load_time = modattr[3]

			# stat the file to determine last modification time
			if (load_time < int(os.stat(modpath).st_mtime)):
				load(name, directory, group_name)
		return

	# reload ALL changed modules
	slog.info("Reloading all modules.")
	for (group_name, group) in module_map.iteritems():
		for (modpath, modattr) in group.iteritems():
			# extract module attributes
			name = modattr[1]
			directory = modattr[2]
			load_time = modattr[3]

			# stat the file to determine last modification time
			if (load_time < int(os.stat(modpath).st_mtime)):
				load(name, directory, group_name, True)
