"""
Database interface.

read_queries() -- read queries from a query package
connect() -- create connection(s) to the database
default_connection() -- get a default connection object
execute() -- execute a SQL query
commit() -- store changes
"""

import os, re, threading, time, hashlib

import slog, modules

# SQL query modules
queries = {}

# keep track of those engine modules that have already been imported
engines = {}

# conn_map --
#	{"master": [connection_list, parameters, free_conn_cond, num_free], ..}
# connection_list -- [(conn_obj, thread_id), ..]
# parameters -- {
# 	"db_type": db_type,
# 	"host": database host,
# 	"user": username,
#	"database": database name (on the server),
# 	"password": password
#	}
# db_type -- database type ("sqlite", "mysql", etc)
# free_conn_cond -- thread waits on this condition until another
# 	thread signals that a connection has been freed and
#	num_free incremented
# num_free -- number of free 'physical' connections for this 'logical'
#	connection

conn_map = {}

# synchronize access to conn_map
conn_map_lock = threading.Lock()

# thread-local storage
td = threading.local()

# python-style parameter matching regexp
param_re = re.compile("%\((\w+)\)[sif]")

class Row:
	"""Result row with named attributes."""

	def __str__(self):
		s = ""
		for key, value in self.__dict__.iteritems():
			s += "%s: %s\n" % (key, value)
		return s

	def __repr__(self):
		return repr(self.__dict__)

class Cursor:
	"""DB API-2.0 cursor that returns result rows with named attributes
	instead of just plain tuples.
	"""
	
	def __init__(self, cursor):
		self.cursor = cursor	# the uderlying real db-api 2.0 cursor

	def close(self):
		self.cursor.close()

	def execute(self, operation, parameters):
		c = self.cursor
		c.execute(operation, parameters)

		# extract some important cursor attributes
		self.description = c.description
		self.rowcount = c.rowcount
		self.lastrowid = c.lastrowid
		self.operation = operation
		self.parameters = parameters
		
	def fetchone(self, cache = False, expiry = 1):
		c = self.cursor
		try:
			row = c.fetchone()
		except:
			return None
		if row == None:
			# also empty results must be cached
			# distunguish between None/False by using string "None"
			if 'mc' in globals() and cache == True:
				key = derive_key(self.operation, self.parameters)
				slog.db("Caching empty result with key", key)
				mc.set(key, "None", expiry)
			return None

		# set column names
		my_row = Row()
		for i in xrange(len(c.description)):
			col = c.description[i]
			name = col[0]
			setattr(my_row, name, row[i])
		slog.verb("Result row:", my_row)
		
		if 'mc' in globals() and cache == True:
			key = derive_key(self.operation, self.parameters)
			slog.db("Caching with key", key)
			mc.set(key, my_row, expiry)
		return my_row

	def fetchall(self, cache = False, expiry = 1):
		c = self.cursor
		rows = c.fetchall()
		my_rows = []

		# set column names for each row
		for row in rows:
			my_row = Row()
			for i in xrange(len(c.description)):
				col = c.description[i]
				name = col[0]
				setattr(my_row, name, row[i])
			my_rows.append(my_row)
		slog.verb("Result rows:", my_rows)

		if 'mc' in globals() and cache == True:
			key = derive_key(self.operation, self.parameters)
			slog.db("Caching with key", key)
			mc.set(key, my_rows, expiry)
		return my_rows

	def __iter__(self):
		return self

	def next(self):
		"""Iteration support for our cursor."""
		row = self.fetchone()
		if row == None:
			raise StopIteration
		return row

	def __del__(self):
		self.cursor.close()

class McCursor:
	""" Simulated cursor which is created from memcache object """
	
	def fetchone(self, x = False, y = False):
		row = self.data
		if row == None:
			return None

		my_row = Row()
		my_row = row
		slog.verb("Result row from memcache:", my_row)
		return my_row

	def fetchall(self, x = False, y = False):
		rows = self.data
		my_rows = []
		
		for row in rows:
			my_row = Row()
			my_row = row
			my_rows.append(my_row)
		slog.verb("Result rows from memcache:", my_rows)
		return my_rows

def read_queries(package_dir):
	"""Read SQL queries from a query package.

	package_dir -- path to the directory that contains the query package
	"""
	# get the SQL query files from package_dir directory
	slog.db("SQL directory:", package_dir, os.listdir(package_dir))
	num_read = 0
	for filename in os.listdir(package_dir):
		if filename == "sql_common.py":
			dbname = "common"
		elif filename == "sql_mysql.py":
			dbname = "mysql"
		elif filename == "sql_postgresql.py":
			dbname = "postgresql"
		elif filename == "sql_sqlite.py":
			dbname = "sqlite"
		elif filename == "sql_oracle.py":
			dbname = "oracle"
		else:
			continue

		# load the query module & make it globally accessible
		modname = filename[:-3]
		query_module = modules.load(modname, package_dir, "db")
		queries[dbname] = query_module

		num_read += 1

	if num_read == 0:
		slog.warn("No SQL files read.")

def get_query(name, connection="default"):
	# get database type from connection parameters
	(x, conn_params, x, x) = conn_map[connection]
	db_type = conn_params["db_type"]
	
	# check if we've got a query for a specific DB type
	if (db_type in queries) and hasattr(queries[db_type], name):
		return getattr(queries[db_type], name)

	return getattr(queries["common"], name)

def connect(db_type, name="default", host="localhost", user="", database="",
	password="", maxconn=3, memcached=False, memcached_host = "localhost",
	memcached_port = "11211", port=None, reconnect_timeout=5,
	connect_timeout=60, charset=""):
	"""Create a database connection(s).

	Create a database connection (may actually create many connections and
	store them in a connection pool).

	db_type -- database type (e.g., 'postgresql', 'mysql')
	name --	give a name to this connection so that later we can refer to it
		by name instead of passing a reference to the connection
		object. Examples would be 'master', 'slave'.
	host, user, database, password -- connection parameters
	"""
	# put all the connection parameters into a dictionary
	conn_params = {
		"name": name,
		"db_type": db_type,
		"host": host,
		"user": user,
		"database": database,
		"password": password,
		"port": port,
		"reconnect_timeout": reconnect_timeout,
		"connect_timeout": connect_timeout,
		"charset": charset
	}

	(engine, conn) = _connect(conn_params)

	# save a reference to the DB-API 2.0 engine module
	engines[db_type] = engine

	# import memcached
	if memcached:
		slog.db("Loading memcache support")
		try:
			mc_string = memcached_host + ':' + str(memcached_port)
			import memcache
			global mc
			slog.db("Connecting to memcache server", mc_string)
			mc = memcache.Client([mc_string], debug=1)
		except:
			slog.err("Cannot load python-memcached module, memcache support disabled")

	conn_map_lock.acquire()
	
	# save the "logical" connection with the requested name
	if name not in conn_map:
		# threads wait on this condition until a connection becomes
		# available
		free_conn_cond = threading.Condition(conn_map_lock)

		# [connection_list, x, x, num_of_free_connections]
		conn_map[name] = [[], conn_params, free_conn_cond, 0]
		
	# append one physical connection to the logical
	conn_map[name][0].append([conn, None])
	conn_map[name][3] += 1
	
	conn_map_lock.release()

	return True

def _connect(conn_params):
	"""Connect to a database and return a tuple consisting of a reference
	to the DB-API 2.0 compatible module and the new connection object.
	"""
	# extract conection parameters
	name = conn_params["name"]
	db_type = conn_params["db_type"]
	host = conn_params["host"]
	user = conn_params["user"]
	database = conn_params["database"]
	password = conn_params["password"]
	port = conn_params["port"]
	connect_timeout = conn_params["connect_timeout"]
	charset = conn_params["charset"]

	# import the requested db-api 2.0 module and connect to the database
	slog.db("Connecting to a", db_type, "database (%s)" % name)
	slog.db("Host:", host, "User:", user, "DB:", database, "Password:",
	    password)
	if db_type == "postgresql":
		import psycopg2 as engine
		paramstr = "host=%s user=%s dbname=%s password=%s"
		paramstr %= (host, user, database, password)
		conn = engine.connect(paramstr)
	elif db_type == "mysql":
		import MySQLdb as engine
		if not port:
			port = 3306
		conn = engine.connect(host=host, user=user, db=database,
		    passwd=password, port=port, connect_timeout=connect_timeout, charset=charset)
	elif db_type == "sqlite":
		import pysqlite2.dbapi2 as engine
		engine.paramstyle = "named"
		detect_types = (engine.PARSE_DECLTYPES | engine.PARSE_COLNAMES)
		conn = engine.connect(database=database,
		    detect_types=detect_types, check_same_thread=False)
	else:
		raise Exception("Unknown database type: " + db_type)

	return (engine, conn)

def _reconnect(conn_name, conn):
	"""Try to reconnect a physical connection. Return the new connection
	object.

	NOTE: This function may only be called from a thread that already owns
	the connection that it wishes to reconnect.

	conn_name -- name of the logical connection
	conn -- the physical connection object
	"""
	# shut off the previous connection
	# conn.close()
	
	conn_map_lock.acquire()

	(conn_list, conn_params, fcc, nf) = conn_map[conn_name]

	# find the connection object we're trying to reconnect..
	for i in xrange(len(conn_list)):
		(conn_obj, thread_obj) = conn_list[i]

		if conn_obj == conn:
			new_conn = None
			while not new_conn:
				try:
					# .. and replace it with a new one
					(engine, new_conn) = _connect(conn_params)
					conn_list[i][0] = new_conn
					break
				except:
					slog.err("Reconnect failed.")
					time.sleep(conn_params["reconnect_timeout"])
					#conn_map_lock.release()
					#raise

	conn_map_lock.release()

	return new_conn

def execute(operation, parameters={}, connection="default", raw=False):
	"""Execute a SQL query operation and return the corresponding cursor
	object.

	operation -- either a SQL query or the name of a SQL query
	parameters -- query variables
	connection -- connection name or object
	raw -- indicates whether operation is a verbatim SQL query or just the
		name of the query in which case it has to be fetched from the
		query package using operation as the name.
	"""
	start = time.time()
	conn_map_lock.acquire()
	
	(conn_list, cparams, free_conn_cond, num_free) = conn_map[connection]
	db_type = cparams["db_type"]

	conn = None
	free_conn_index = -1
	current_thread = threading.currentThread()

	# check if this thread already owns a connection
	for i in xrange(len(conn_list)):
		(conn_obj, thread_obj) = conn_list[i]

		if thread_obj == current_thread:
			conn = conn_obj
			break
		
		if (free_conn_index == -1) and (thread_obj == None):
			free_conn_index = i

	if conn == None:
		if free_conn_index != -1:
			# get the free connection
			conn = conn_list[free_conn_index][0]
			conn_list[free_conn_index][1] = current_thread
			conn_map[connection][3] -= 1
		else:
			while True:
				# wait for a connection to become available
				free_conn_cond.wait()
				
				# check if a free connection has appeared
				if conn_map[connection][3] > 0:
					break

			# find the free connection
			for i in xrange(len(conn_list)):
				(conn_obj, thread_obj) = conn_list[i]
				if thread_obj == None:
					conn = conn_obj
					conn_list[i][1] = current_thread
					conn_map[connection][3] -= 1
					break

	conn_map_lock.release()

	if conn == None:
		raise Exception("should have acquired a valid connection by now")

	# get engine reference
	engine = engines[db_type]

	if raw:
		op = operation
	else:
		# check if we've got a query for a specific DB type
		if (db_type in queries) and hasattr(queries[db_type], operation):
			op = getattr(queries[db_type], operation)
		else:
			op = getattr(queries["common"], operation)

	# replace python-style parameters with whatever the engine uses
	if engine.paramstyle == "named":
		op_temp = op
		op = ""
		while True:
			mo = param_re.search(op_temp)
			if mo == None:
				op += op_temp
				break

			op += op_temp[:mo.start()]
			op += ":" + mo.group(1)
			op_temp = op_temp[mo.end():]

	# replace the string ``%(prefix)'' with the table prefix if it was set
	if hasattr(td, "prefix"):
		op = op.replace("%(prefix)", td.prefix)

	# convert parameters to a dictionary
	if not isinstance(parameters, dict):
		parameters = parameters.__dict__
	
	# log some info about the query
	slog.db("Executing query", operation)
	slog.verb("Executing query", op)
	slog.verb("Query parameters:", parameters)

	if 'mc' in globals():
		key = derive_key(op, parameters)
		obj = mc.get(key)
		if obj:
			slog.db("Using memcache obj with key ",key)
			cursor = McCursor()
			# distunguish between None/False by using string "None"
			if obj == "None":
				cursor.data = None
			else:
				cursor.data = obj
			end = time.time()
			slog.db("Cached Query", operation, "exec-time", end-start, "s")
			return cursor

	# create our wrapper cursor from the original
	db_cur = conn.cursor()
	wrapper_cur = Cursor(db_cur)
	try:
		# execute the query
		wrapper_cur.execute(op, parameters)

	except engine.OperationalError, err:
		# check if this is a "MySQL server has gone away" problem
		# (i.e., server timed out and closed the connection, so we must
		# reconnect and issue the query again)
		if (db_type == "mysql") and (len(err.args) == 2) and \
		    (err.args[0] == 2006):
			# log some warning messages about this
			slog.warn("Connection to MySQL server lost.")
			slog.warn("Trying to reconnect..")

			# get a new connection and a new cursor
			new_conn = _reconnect(connection, conn)
			db_cur = new_conn.cursor()
			wrapper_cur = Cursor(db_cur)

			# execute the query again
			wrapper_cur.execute(op, parameters)
		else:
			#raise
			slog.warn("Execute error", err.args)

	end = time.time()
	slog.db("Query", operation, "exec-time", end-start, "s")
	return wrapper_cur

def derive_key(op, parameters):
	""" Derive memcache key """
	return hashlib.md5(str(op) + repr(parameters.items())).hexdigest()

def commit(connection=None):
	"""Commit a transaction."""
	_stop_transaction(connection, True)

def rollback(connection=None):
	"""Rollback a transaction."""
	_stop_transaction(connection, False)

def _stop_transaction(connection=None, commit=True):
	"""Stop a transaction either by commit() or rollback()."""
	conn_map_lock.acquire()

	current_thread = threading.currentThread()

	if connection != None:
		(conn_list, cparams, fcc, num_free) = conn_map[connection]
		for i in xrange(len(conn_list)):
			(conn_obj, thread_obj) = conn_list[i]
			if thread_obj == current_thread:
				try:
					if commit:
						conn_obj.commit()
					else:
						conn_obj.rollback()
					return
				finally:
					conn_map_lock.release()

	for (conn_name, conn_details) in conn_map.iteritems():
		(conn_list, cparams, fcc, num_free) = conn_details
		for i in xrange(len(conn_list)):
			(conn_obj, thread_obj) = conn_list[i]
			if thread_obj == current_thread:
				try:
					if commit:
						conn_obj.commit()
					else:
						conn_obj.rollback()
				except:
					conn_map_lock.release()
					raise
				break
		
	conn_map_lock.release()

def free_connection(connection=None, commit=True):
	"""Perform commit() or rollback() on a connection, and make the
	connection object available to other threads."""

	conn_map_lock.acquire()

	current_thread = threading.currentThread()

	if connection != None:
		(conn_list, cp, free_conn_cond, nf) = conn_map[connection]
		for i in xrange(len(conn_list)):
			(conn_obj, thread_obj) = conn_list[i]
			if thread_obj == current_thread:
				try:
					if commit:
						conn_obj.commit()
					else:
						conn_obj.rollback()
					return
				finally:
					conn_list[i][1] = None
					conn_map[connection][3] += 1
					free_conn_cond.notify()
					conn_map_lock.release()

	for (conn_name, conn_details) in conn_map.iteritems():
		(conn_list, cp, free_conn_cond, nf) = conn_details
		for i in xrange(len(conn_list)):
			(conn_obj, thread_obj) = conn_list[i]
			if thread_obj == current_thread:
				try:
					if commit:
						conn_obj.commit()
					else:
						conn_obj.rollback()
				except:
					conn_list[i][1] = None
					conn_map[conn_name][3] += 1
					free_conn_cond.notify()
					conn_map_lock.release()
					return

				conn_list[i][1] = None
				conn_map[conn_name][3] += 1
				free_conn_cond.notify()
				break
		
	conn_map_lock.release()

def close(connection="default"):
	"""Perform close() on a connection."""
	
	conn_map_lock.acquire()
	try:
		(conn_list, cparams, fcc, num_free) = conn_map[connection]
	except:
		conn_map_lock.release()
		slog.warn("Connection ", connection, " not found, cannot close")
		return
	
	(conn_obj, thread_obj) = conn_list[0]
	conn_obj.close()
	conn_map_lock.release()
	return

def is_connected(connection="default"):
	"""return true if connection is in the list of connections, false otherwise"""
	conn_map_lock.acquire()
	try:
		conn_map[connection]
		conn_map_lock.release()
		return True
	except:
		conn_map_lock.release()
		return False

def set_prefix(prefix):
	"""Set table prefix.

	After set_prefix() the string ``%(prefix)'' is replaced with prefix in
	each query (a simple text replacement)
	"""
	td.prefix = prefix

def get_prefix():
	if hasattr(td, "prefix"):
		return td.prefix
	else:
		return ''
