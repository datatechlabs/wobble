#!/usr/bin/env python

#
# wobble setup script
#

from distutils.core import setup

dist = setup(
	name = 'wobble',
	version = "2.8.0",
	author = 'DataTechLabs',
	author_email = 'info@datatechlabs.com',
	url = 'http://www.datatechlabs.com',
	packages = ['wobble', 'wobble.server'],
	package_data = {'wobble': ['*.pyc'], 'wobble.server': ['*.pyc']}
)
